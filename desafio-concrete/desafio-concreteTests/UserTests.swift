//
//  UserTests.swift
//  desafio-concreteTests
//
//  Created by Gabriel Alberto de Jesus Preto on 15/01/2018.
//  Copyright © 2018 Gabriel. All rights reserved.
//

import XCTest
import ObjectMapper

@testable import desafio_concrete

class UserTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testConstructor() {
        let userTuple = ("eclipse", "https://avatars1.githubusercontent.com/u/56974?s=200&v=4")
        let json = "{\"login\" : \"\(userTuple.0)\", \"avatar_url\" : \"\(userTuple.1)\"}"
        
        let user = Mapper<User>().map(JSONString: json)
        
        XCTAssertNotNil(user)
        XCTAssertEqual(userTuple.0, user?.username)
        XCTAssertEqual(URL(string: userTuple.1), user?.avatar)
        
    }
    
}
