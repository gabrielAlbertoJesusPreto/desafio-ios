//
//  Repository.swift
//  desafio-concreteTests
//
//  Created by Gabriel Alberto de Jesus Preto on 15/01/2018.
//  Copyright © 2018 Gabriel. All rights reserved.
//

import XCTest
import ObjectMapper

@testable import desafio_concrete

class RepositoryTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testConstructor() {
        let repositoryTuple = ("Java-Lang", "This module provides marshalling, de-marshalling and handling of thread safe off heap memory through ByteBuffers.", 78, 252)
        let json = "{\"name\" : \"\(repositoryTuple.0)\", \"forks_count\" : \(repositoryTuple.2), \"stargazers_count\" : \(repositoryTuple.3), \"description\" : \"\(repositoryTuple.1)\" }"
        
        let repository = Mapper<Repository>().map(JSONString: json)
        
        XCTAssertNotNil(repository)
        XCTAssertEqual(repositoryTuple.0, repository?.name)
        XCTAssertEqual(repositoryTuple.1, repository?.repoDescription)
        XCTAssertEqual(repositoryTuple.2, repository?.forks)
        XCTAssertEqual(repositoryTuple.3, repository?.stars)
    }
    
}
