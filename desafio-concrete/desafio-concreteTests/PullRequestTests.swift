//
//  PullRequest.swift
//  desafio-concreteTests
//
//  Created by Gabriel Alberto de Jesus Preto on 15/01/2018.
//  Copyright © 2018 Gabriel. All rights reserved.
//

import XCTest
import ObjectMapper

@testable import desafio_concrete

class PullRequestTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        let pullRequestTuple = ("vscode intellisense failing when 2 jdks are on system", "Fixes redhat-developer/vscode-java#392", "https://github.com/eclipse/eclipse.jdt.ls/pull/519", "2018-01-15 22:24:25")
        let json = "{\"title\" : \"\(pullRequestTuple.0)\", \"body\" : \"\(pullRequestTuple.1)\", \"html_url\" : \"\(pullRequestTuple.2)\", \"created_at\" : \"\(pullRequestTuple.3)\" }"
        
        let pullRequest = Mapper<PullRequest>().map(JSONString: json)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = dateFormatter.date(from: pullRequestTuple.3)
        
        XCTAssertNotNil(pullRequest)
        XCTAssertEqual(pullRequestTuple.0, pullRequest?.title)
        XCTAssertEqual(pullRequestTuple.1, pullRequest?.prDescription)
        XCTAssertEqual(URL(string: pullRequestTuple.2), pullRequest?.url)
        XCTAssertEqual(date, pullRequest?.date)
    }
    
}
