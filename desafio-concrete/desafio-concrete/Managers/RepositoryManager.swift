//
//  RepositoryManager.swift
//  desafio-concrete
//
//  Created by Gabriel Alberto de Jesus Preto on 14/01/2018.
//  Copyright © 2018 Gabriel. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

protocol RepositoryManagerDelegate {
    func dataReady()
}

class RepositoryManager: NSObject {
    
    var repositories = [Repository]()
    
    var delegate: RepositoryManagerDelegate?

    let URL_PATH = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page="
    var pageCount = 1
    
    func getRepositories() {
        Alamofire.request(URL(string: "\(URL_PATH)\(pageCount)")!).validate().responseJSON {(response) in
            
            if response.result.error != nil {
                return
            }
            
            guard let data = response.result.value as? [String : AnyObject] else {
                print("\(#function) - \(#line) line - Repository request error")
                return
            }
            
            guard let items = data["items"] as? [[String : AnyObject]] else {
                print("\(#function) - \(#line) line - Repository request error")
                return
            }
            
            for item in items {
                guard let repository = Mapper<Repository>().map(JSON: item as [String : Any]) else {
                    continue
                }

                self.repositories.append(repository)

                if self.delegate != nil {
                    self.delegate!.dataReady()
                }
            }
        }
        
        self.pageCount += 1
    }
    
}
