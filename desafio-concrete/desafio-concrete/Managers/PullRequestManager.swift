//
//  PullRequestManager.swift
//  desafio-concrete
//
//  Created by Gabriel Alberto de Jesus Preto on 15/01/2018.
//  Copyright © 2018 Gabriel. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

protocol PullRequestManagerDelegate {
    func dataReady()
    func searchWasEmpty()
    func searchIsFinished()
}

class PullRequestManager: NSObject {
    
    var pullRequests = [PullRequest]()
    
    var delegate: PullRequestManagerDelegate?
    
    func getPullRequests(user: String, repository: String) {
        
        let URL_PATH = "https://api.github.com/repos/\(user)/\(repository)/pulls"
        
        Alamofire.request(URL(string: URL_PATH)!).validate().responseJSON {(response) in
    
            if response.result.error != nil {
                print("\(#function) - \(#line) line - Pull Request request error")
                return
            }
            
            guard let items = response.result.value as? [[String : AnyObject]] else {
                print("\(#function) - \(#line) line - Pull Request request error")
                return
            }

            if items.isEmpty {
                if self.delegate != nil {
                    self.delegate!.searchWasEmpty()
                }
            }
            
            for item in items {
                guard let pullRequest = Mapper<PullRequest>().map(JSON: item as [String : Any]) else {
                    continue
                }
                self.pullRequests.append(pullRequest)

                if self.delegate != nil {
                    self.delegate!.dataReady()
                }
            }
            
            if self.delegate != nil {
                 self.delegate!.searchIsFinished()
            }
        }
    }
}
