//
//  PullRequestsTableViewController.swift
//  desafio-concrete
//
//  Created by Gabriel Alberto de Jesus Preto on 13/01/2018.
//  Copyright © 2018 Gabriel. All rights reserved.
//

import UIKit
import Kingfisher
import ProgressHUD

class PullRequestsTableViewController: UITableViewController, PullRequestManagerDelegate {
    @IBOutlet weak var prOpenedLabel: UILabel!
    
    var pullRequests = [PullRequest]()
    let pullRequestManager = PullRequestManager()
    
    var user: String = ""
    var repositoryName: String = "" {
        didSet {
            self.title = repositoryName
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.pullRequestManager.delegate = self
        
        if !Reachability().networkVerification() {
            ProgressHUD.showError("Sem conexão")
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        self.pullRequestManager.getPullRequests(user: self.user, repository: self.repositoryName)
        ProgressHUD.show()
        
        self.prOpenedLabel.text = "Carregando..."
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        ProgressHUD.dismiss()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pullRequests.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let pullRequest = self.pullRequests[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "PullRequestCell", for: indexPath) as! PullRequestsTableViewCell
        cell.titleLabel.text = pullRequest.title
        cell.descriptionLabel.text = pullRequest.prDescription
        cell.dateLabel.text = self.convertDateToString(date: pullRequest.date!)
        cell.userUsernameLabel.text = pullRequest.owner?.username
        self.getImageFromUrl(url: (pullRequest.owner?.avatar)!, imageView: cell.avatarImage)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let url = self.pullRequests[indexPath.row].url
        UIApplication.shared.open(url!, options: [:], completionHandler: nil)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170
    }
    
    // MARK: PullRequestManager Delegate
    
    func dataReady() {
        self.pullRequests = self.pullRequestManager.pullRequests
        self.tableView.reloadData()
        ProgressHUD.dismiss()
    }
    
    func searchWasEmpty() {
        ProgressHUD.dismiss()
        let alertController = UIAlertController(title: "Busca Vazia!", message: "Não existe pull requests para este Repositório", preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default) { (action) in
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(alertAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func searchIsFinished() {
        self.prOpenedLabel.text = "\(self.pullRequests.count) Pull Requests opened"
    }
    
    // MARK: Helper Methods
    
    func getImageFromUrl(url: URL, imageView: UIImageView) {
        imageView.image = UIImage(named: "avatar")
        DispatchQueue(label: "ImageQueue").async {
            DispatchQueue.main.async {
                imageView.kf.indicatorType = .activity
                imageView.kf.setImage(with: url)
            }
        }
    }
    
    func convertDateToString(date: Date) -> String {
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)-2000
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        
        return "\(day)/\(month)/\(year)"
    }
}

