//
//  RepositoriesTableViewController.swift
//  desafio-concrete
//
//  Created by Gabriel Alberto de Jesus Preto on 13/01/2018.
//  Copyright © 2018 Gabriel. All rights reserved.
//

import UIKit
import Kingfisher
import ProgressHUD

class RepositoriesTableViewController: UITableViewController, RepositoryManagerDelegate {
    
    var repositories = [Repository]()
    let repositoryManager = RepositoryManager()
    
    let threshold: Double = 100.0
    var isLoadingMore = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Github JavaPop"
        
        self.repositoryManager.delegate = self
        
        if !Reachability().networkVerification() {
            ProgressHUD.showError("Sem conexão")
            return
        }
        
        self.repositoryManager.getRepositories()
        ProgressHUD.show()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.repositories.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let repository = self.repositories[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepositoryCell", for: indexPath) as! RepositoriesTableViewCell
        cell.nameLabel.text = repository.name
        cell.descriptionLabel.text = repository.repoDescription
        cell.forksLabel.text = String(describing: repository.forks!)
        cell.starsLabel.text = String(describing: repository.stars!)
        cell.userUsernameLabel.text = repository.owner?.username
        self.getImageFromUrl(url: (repository.owner?.avatar)!, imageView: cell.userImage)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 164
    }
    
    // MARK: Table view delegate
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
        let x = (Double(maximumOffset - contentOffset))
        
        if !isLoadingMore && (x <= threshold) {
            self.isLoadingMore = true
            self.repositoryManager.getRepositories()

            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.isLoadingMore = false
            }
        }
    }
    
    // MARK: RepositoryManager Delegate
    
    func dataReady() {
        self.repositories = self.repositoryManager.repositories
        self.tableView.reloadData()
        ProgressHUD.dismiss()
    }
    
    // MARK: Helper Methods
    
    func getImageFromUrl(url: URL, imageView: UIImageView) {
        imageView.image = UIImage(named: "avatar")
        DispatchQueue(label: "ImageQueue").async {
            DispatchQueue.main.async {
                imageView.kf.indicatorType = .activity
                imageView.kf.setImage(with: url)
            }
        }
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToPullRequest" {
            let viewController = segue.destination as! PullRequestsTableViewController
            viewController.user = (repositories[(tableView.indexPathForSelectedRow?.row)!].owner?.username)!
            viewController.repositoryName = (repositories[(tableView.indexPathForSelectedRow?.row)!].name)!
        }
    }


}
