//
//  RepositoriesTableViewCell.swift
//  desafio-concrete
//
//  Created by Gabriel Alberto de Jesus Preto on 13/01/2018.
//  Copyright © 2018 Gabriel. All rights reserved.
//

import UIKit

class RepositoriesTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var forksLabel: UILabel!
    @IBOutlet weak var starsLabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userUsernameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.userImage.layer.borderWidth = 0.5
        self.userImage.layer.borderColor = UIColor.lightGray.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
