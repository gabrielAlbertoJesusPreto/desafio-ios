//
//  User.swift
//  desafio-concrete
//
//  Created by Gabriel Alberto de Jesus Preto on 13/01/2018.
//  Copyright © 2018 Gabriel. All rights reserved.
//

import Foundation
import ObjectMapper

class User: Mappable {
    
    // MARK: Attributes
    
    var avatar: URL?
    var username: String?
    
    // MARK: Constructors
    
    required init?(map: Map) {
        
    }
    
    init(username: String, avatar: URL) {
        self.username = username
        self.avatar = avatar
    }
    
    // MARK: Mappable Protocol
    
    func mapping(map: Map) {
        self.avatar <- (map["avatar_url"], URLTransform())
        self.username <- map["login"]
    }
    
}
