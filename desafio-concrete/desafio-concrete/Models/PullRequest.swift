//
//  PullRequest.swift
//  desafio-concrete
//
//  Created by Gabriel Alberto de Jesus Preto on 14/01/2018.
//  Copyright © 2018 Gabriel. All rights reserved.
//

import Foundation
import ObjectMapper

class PullRequest: Mappable {
    
    // MARK: Attributes
    
    var title: String?
    var prDescription: String?
    var date: Date?
    var url: URL?
    var owner: User?
    
    // MARK: Constructors
    
    required init?(map: Map) {
        
    }
    
    init(title: String, prDescription: String, url: URL, date: Date, owner: User) {
        self.title = title
        self.prDescription = prDescription
        self.url = url
        self.owner = owner
        self.date = date
    }
    
    // MARK: Mappable Protocol
    
    func mapping(map: Map) {
        self.title <- map["title"]
        self.prDescription <- map["body"]
        self.url <- (map["html_url"], URLTransform())
        self.owner <- map["user"]
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        if let dateString = map["created_at"].currentValue as? String, let date = dateFormatter.date(from: dateString) {
            self.date = date
        }
    }
}
