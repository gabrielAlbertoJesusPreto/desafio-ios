//
//  Repository.swift
//  desafio-concrete
//
//  Created by Gabriel Alberto de Jesus Preto on 13/01/2018.
//  Copyright © 2018 Gabriel. All rights reserved.
//

import Foundation
import ObjectMapper

class Repository: Mappable {
    
    // MARK: Attributes

    var name: String?
    var repoDescription: String?
    var forks: Int?
    var stars: Int?
    var owner: User?
    
    // MARK: Constructors
    
    required init?(map: Map) {
        
    }
    
    init(name: String, description: String, forks: Int, stars: Int, owner: User) {
        self.name = name
        self.repoDescription = description
        self.forks = forks
        self.stars = stars
        self.owner = owner
    }
    
    // MARK: Mapple Protocol
    
    func mapping(map: Map) {
        self.name <- map["name"]
        self.repoDescription <- map["description"]
        self.forks <- map["forks_count"]
        self.stars <- map["stargazers_count"]
        self.owner <- map["owner"]
    }
}
